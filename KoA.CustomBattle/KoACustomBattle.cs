﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using KoA.Core.Models;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.ModuleManager;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.CustomBattle;
using TaleWorlds.ObjectSystem;

namespace KoA.CustomBattle
{
    public class KoACustomBattle : GameType
    {
        private const TerrainType DefaultTerrain = TerrainType.Plain;
        private const ForestDensity DefaultForestDensity = ForestDensity.None;
        private readonly List<CustomBattleSceneData> _customBattleScenes;

        public KoACustomBattle()
        {
            _customBattleScenes = new List<CustomBattleSceneData>();
        }

        public IEnumerable<CustomBattleSceneData> CustomBattleScenes => _customBattleScenes;

        public override bool IsCoreOnlyGameMode => true;

        public static KoACustomBattle Current => Game.Current.GameType as KoACustomBattle;

        protected override void OnInitialize()
        {
            InitializeScenes();
            var currentGame = CurrentGame;
            InitializeGameTexts(currentGame.GameTextManager);
            IGameStarter gameStarter = new BasicGameStarter();
            InitializeGameModels(gameStarter);
            GameManager.InitializeGameStarter(currentGame, gameStarter);
            GameManager.OnGameStart(CurrentGame, gameStarter);
            var objectManager = currentGame.ObjectManager;
            currentGame.SetBasicModels(gameStarter.Models);
            currentGame.CreateGameManager();
            GameManager.BeginGameStart(CurrentGame);
            CurrentGame.SetRandomGenerators();
            currentGame.InitializeDefaultGameObjects();
            currentGame.LoadBasicFiles();
            LoadCustomGameXmls();
            objectManager.UnregisterNonReadyObjects();
            currentGame.SetDefaultEquipments(
                new Dictionary<string, Equipment>());
            objectManager.UnregisterNonReadyObjects();
            GameManager.OnNewCampaignStart(CurrentGame, null);
            GameManager.OnAfterCampaignStart(CurrentGame);
            GameManager.OnGameInitializationFinished(CurrentGame);
        }

        private void InitializeGameModels(IGameStarter basicGameStarter)
        {
            basicGameStarter.AddModel(new MultiplayerAgentDecideKilledOrUnconsciousModel());
            basicGameStarter.AddModel(new CustomBattleAgentStatCalculateModel());
            basicGameStarter.AddModel(new DefaultMissionDifficultyModel());
            basicGameStarter.AddModel(new CustomBattleApplyWeatherEffectsModel());
            basicGameStarter.AddModel(new CustomBattleAutoBlockModel());
            basicGameStarter.AddModel(new MultiplayerAgentApplyDamageModel());
            basicGameStarter.AddModel(new DefaultRidingModel());
            basicGameStarter.AddModel(new DefaultStrikeMagnitudeModel());
            basicGameStarter.AddModel(new CustomBattleMoraleModel());
            basicGameStarter.AddModel(new CustomBattleInitializationModel());
            basicGameStarter.AddModel(new KoADamageParticleModel());
        }

        private void InitializeGameTexts(GameTextManager gameTextManager)
        {
            gameTextManager.LoadGameTexts(ModuleHelper.GetModuleFullPath("Native") +
                                          "ModuleData/multiplayer_strings.xml");
            gameTextManager.LoadGameTexts(ModuleHelper.GetModuleFullPath("Native") + "ModuleData/global_strings.xml");
            gameTextManager.LoadGameTexts(ModuleHelper.GetModuleFullPath("Native") + "ModuleData/module_strings.xml");
            gameTextManager.LoadGameTexts(ModuleHelper.GetModuleFullPath("Native") + "ModuleData/native_strings.xml");
        }

        private void InitializeScenes()
        {
            LoadCustomBattleScenes(MBObjectManager.GetMergedXmlForManaged("Scene", true));
        }

        private void LoadCustomGameXmls()
        {
            ObjectManager.LoadXML("Items");
            ObjectManager.LoadXML("EquipmentRosters");
            ObjectManager.LoadXML("NPCCharacters");
            ObjectManager.LoadXML("SPCultures");
        }

        protected override void BeforeRegisterTypes(MBObjectManager objectManager)
        {
        }

        protected override void OnRegisterTypes(MBObjectManager objectManager)
        {
            objectManager.RegisterType<BasicCharacterObject>("NPCCharacter", "NPCCharacters", 43U);
            objectManager.RegisterType<BasicCultureObject>("Culture", "SPCultures", 17U);
        }

        protected override void DoLoadingForGameType(
            GameTypeLoadingStates gameTypeLoadingState,
            out GameTypeLoadingStates nextState)
        {
            nextState = GameTypeLoadingStates.None;
            switch (gameTypeLoadingState)
            {
                case GameTypeLoadingStates.InitializeFirstStep:
                    CurrentGame.Initialize();
                    nextState = GameTypeLoadingStates.WaitSecondStep;
                    break;
                case GameTypeLoadingStates.WaitSecondStep:
                    nextState = GameTypeLoadingStates.LoadVisualsThirdState;
                    break;
                case GameTypeLoadingStates.LoadVisualsThirdState:
                    nextState = GameTypeLoadingStates.PostInitializeFourthState;
                    break;
            }
        }

        public override void OnDestroy()
        {
        }

        private XmlDocument LoadXmlFile(string path)
        {
            Debug.Print("opening " + path);
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(new StreamReader(path).ReadToEnd());
            return xmlDocument;
        }

        private void LoadCustomBattleScenes(XmlDocument doc)
        {
            if (doc.ChildNodes.Count == 0)
                throw new TWXmlLoadException("Incorrect XML document format. XML document has no nodes.");
            var num = doc.ChildNodes[0].Name.ToLower().Equals("xml") ? 1 : 0;

            if (num != 0 && doc.ChildNodes.Count == 1)
                throw new TWXmlLoadException(
                    "Incorrect XML document format. XML document must have at least one child node");
            var xmlNode1 = num != 0 ? doc.ChildNodes[1] : doc.ChildNodes[0];

            if (xmlNode1.Name != "CustomBattleScenes")
                throw new TWXmlLoadException(
                    "Incorrect XML document format. Root node's name must be CustomBattleScenes.");

            if (!(xmlNode1.Name == "CustomBattleScenes"))
                return;

            foreach (XmlNode childNode1 in xmlNode1.ChildNodes)
                if (childNode1.NodeType != XmlNodeType.Comment)
                {
                    string sceneID = null;
                    TextObject name = null;
                    var result1 = TerrainType.Plain;
                    var result2 = ForestDensity.None;
                    var result3 = false;
                    var result4 = false;
                    var result5 = false;
                    for (var i = 0; i < childNode1.Attributes.Count; ++i)
                        if (childNode1.Attributes[i].Name == "id")
                        {
                            sceneID = childNode1.Attributes[i].InnerText;
                        }
                        else if (childNode1.Attributes[i].Name == "name")
                        {
                            name = new TextObject(childNode1.Attributes[i].InnerText);
                        }
                        else if (childNode1.Attributes[i].Name == "terrain")
                        {
                            if (!Enum.TryParse(childNode1.Attributes[i].InnerText, out result1))
                                result1 = TerrainType.Plain;
                        }
                        else if (childNode1.Attributes[i].Name == "forest_density")
                        {
                            var charArray = childNode1.Attributes[i].InnerText.ToLower().ToCharArray();
                            charArray[0] = char.ToUpper(charArray[0]);
                            if (!Enum.TryParse(new string(charArray), out result2))
                                result2 = ForestDensity.None;
                        }
                        else if (childNode1.Attributes[i].Name == "is_siege_map")
                        {
                            bool.TryParse(childNode1.Attributes[i].InnerText, out result3);
                        }
                        else if (childNode1.Attributes[i].Name == "is_village_map")
                        {
                            bool.TryParse(childNode1.Attributes[i].InnerText, out result4);
                        }
                        else if (childNode1.Attributes[i].Name == "is_lords_hall_map")
                        {
                            bool.TryParse(childNode1.Attributes[i].InnerText, out result5);
                        }

                    var childNodes = childNode1.ChildNodes;
                    var terrainTypes = new List<TerrainType>();
                    foreach (XmlNode xmlNode2 in childNodes)
                        if (xmlNode2.NodeType != XmlNodeType.Comment && xmlNode2.Name == "flags")
                            foreach (XmlNode childNode2 in xmlNode2.ChildNodes)
                            {
                                TerrainType result6;
                                if (childNode2.NodeType != XmlNodeType.Comment &&
                                    childNode2.Attributes["name"].InnerText == "TerrainType" &&
                                    Enum.TryParse(childNode2.Attributes["value"].InnerText, out result6) &&
                                    !terrainTypes.Contains(result6))
                                    terrainTypes.Add(result6);
                            }

                    _customBattleScenes.Add(new CustomBattleSceneData(sceneID, name, result1, terrainTypes,
                        result2, result3, result4, result5));
                }
        }

        public override void OnStateChanged(GameState oldState)
        {
        }
    }
}