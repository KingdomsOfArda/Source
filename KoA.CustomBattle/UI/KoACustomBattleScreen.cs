﻿using System.Linq;
using TaleWorlds.Core;
using TaleWorlds.Engine;
using TaleWorlds.Engine.GauntletUI;
using TaleWorlds.Engine.Screens;
using TaleWorlds.GauntletUI.Data;
using TaleWorlds.InputSystem;
using TaleWorlds.MountAndBlade.View.Screen;

namespace KoA.CustomBattle.UI
{
    [GameStateScreen(typeof(KoACustomBattleState))]
    public class KoACustomBattleScreen : ScreenBase, IGameStateListener
    {
        private readonly KoACustomBattleState _customBattleState;
        private KoACustomBattleMenuVM _dataSource;
        private GauntletLayer _gauntletLayer;
        private IGauntletMovie _gauntletMovie;
        private bool _isMovieLoaded;

        public KoACustomBattleScreen(KoACustomBattleState customBattleState)
        {
            _customBattleState = customBattleState;
        }

        void IGameStateListener.OnActivate()
        {
        }

        void IGameStateListener.OnDeactivate()
        {
        }

        void IGameStateListener.OnInitialize()
        {
        }

        void IGameStateListener.OnFinalize()
        {
            _dataSource.OnFinalize();
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            _dataSource = new KoACustomBattleMenuVM(_customBattleState);
            _dataSource.SetStartInputKey(HotKeyManager.GetCategory("GenericCampaignPanelsGameKeyCategory")
                .RegisteredHotKeys.FirstOrDefault(g => g?.Id == "Start"));
            _dataSource.SetCancelInputKey(HotKeyManager.GetCategory("GenericCampaignPanelsGameKeyCategory")
                .RegisteredHotKeys.FirstOrDefault(g => g?.Id == "Exit"));
            _dataSource.SetRandomizeInputKey(HotKeyManager.GetCategory("GenericCampaignPanelsGameKeyCategory")
                .RegisteredHotKeys.FirstOrDefault(g => g?.Id == "Randomize"));
            _dataSource.TroopTypeSelectionPopUp?.SetDoneInputKey(HotKeyManager
                .GetCategory("GenericCampaignPanelsGameKeyCategory").RegisteredHotKeys
                .FirstOrDefault(g => g?.Id == "Confirm"));
            _gauntletLayer = new GauntletLayer(1);
            _gauntletLayer.Input.RegisterHotKeyCategory(
                HotKeyManager.GetCategory("GenericCampaignPanelsGameKeyCategory"));
            LoadMovie();
            _gauntletLayer.InputRestrictions.SetInputRestrictions();
            _dataSource.SetActiveState(true);
            AddLayer(_gauntletLayer);
        }

        protected override void OnFrameTick(float dt)
        {
            base.OnFrameTick(dt);
            if (_gauntletLayer.IsFocusedOnInput())
                return;
            var typeSelectionPopUp = _dataSource.TroopTypeSelectionPopUp;
            if ((typeSelectionPopUp != null ? typeSelectionPopUp.IsOpen ? 1 : 0 : 0) != 0)
            {
                if (_gauntletLayer.Input.IsHotKeyDownAndReleased("Exit"))
                {
                    _dataSource.TroopTypeSelectionPopUp.ExecuteCancel();
                }
                else
                {
                    if (!_gauntletLayer.Input.IsHotKeyDownAndReleased("Confirm"))
                        return;
                    _dataSource.TroopTypeSelectionPopUp.ExecuteDone();
                }
            }
            else if (_gauntletLayer.Input.IsHotKeyDownAndReleased("Exit"))
            {
                _dataSource.ExecuteBack();
            }
            else if (_gauntletLayer.Input.IsHotKeyDownAndReleased("Randomize"))
            {
                _dataSource.ExecuteRandomize();
            }
            else
            {
                if (!_gauntletLayer.Input.IsHotKeyDownAndReleased("Start"))
                    return;
                _dataSource.ExecuteStart();
            }
        }

        protected override void OnFinalize()
        {
            UnloadMovie();
            RemoveLayer(_gauntletLayer);
            _dataSource = null;
            _gauntletLayer = null;
            base.OnFinalize();
        }

        protected override void OnActivate()
        {
            LoadMovie();
            _dataSource?.SetActiveState(true);
            _gauntletLayer.IsFocusLayer = true;
            ScreenManager.TrySetFocus(_gauntletLayer);
            LoadingWindow.DisableGlobalLoadingWindow();
            base.OnActivate();
        }

        protected override void OnDeactivate()
        {
            base.OnDeactivate();
            UnloadMovie();
            _dataSource?.SetActiveState(false);
        }

        private void LoadMovie()
        {
            if (_isMovieLoaded)
                return;
            _gauntletMovie = _gauntletLayer.LoadMovie(nameof(KoACustomBattleScreen), _dataSource);
            //_gauntletMovie = _gauntletLayer.LoadMovie("CustomBattleScreen", _dataSource);
            _isMovieLoaded = true;
        }

        private void UnloadMovie()
        {
            if (!_isMovieLoaded)
                return;
            _gauntletLayer.ReleaseMovie(_gauntletMovie);
            _gauntletMovie = null;
            _isMovieLoaded = false;
            _gauntletLayer.IsFocusLayer = false;
            ScreenManager.TryLoseFocus(_gauntletLayer);
        }
    }
}