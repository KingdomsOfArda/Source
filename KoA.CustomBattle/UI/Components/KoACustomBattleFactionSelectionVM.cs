﻿using System;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle.SelectionItem;

namespace KoA.CustomBattle.UI.Components
{
    public class KoACustomBattleFactionSelectionVM : ViewModel
    {
        private readonly Action<BasicCultureObject> _onSelectionChanged;
        private MBBindingList<FactionItemVM> _factions;
        private string _selectedFactionName;
        public FactionItemVM SelectedItem;

        public KoACustomBattleFactionSelectionVM(Action<BasicCultureObject> onSelectionChanged)
        {
            _onSelectionChanged = onSelectionChanged;
            Factions = new MBBindingList<FactionItemVM>();

            foreach (var faction in KoACustomBattleData.Factions)
                Factions.Add(new FactionItemVM(faction, OnFactionSelected));

            SelectedItem = Factions[0];
            SelectFaction(0);
            Factions.ApplyActionOnAllItems(x => x.RefreshValues());
        }

        [DataSourceProperty]
        public MBBindingList<FactionItemVM> Factions
        {
            get => _factions;
            set
            {
                if (value == _factions)
                    return;
                _factions = value;
                OnPropertyChangedWithValue(value, nameof(Factions));
            }
        }

        [DataSourceProperty]
        public string SelectedFactionName
        {
            get => _selectedFactionName;
            set
            {
                if (!(value != _selectedFactionName))
                    return;
                _selectedFactionName = value;
                OnPropertyChangedWithValue(value, nameof(SelectedFactionName));
            }
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            SelectedFactionName = SelectedItem?.Faction.Name.ToString();
        }

        public void SelectFaction(int index)
        {
            if ((index < 0 ? 0 : index < Factions.Count ? 1 : 0) == 0)
                return;
            SelectedItem.IsSelected = false;
            SelectedItem = Factions[index];
            SelectedItem.IsSelected = true;
        }

        public void ExecuteRandomize()
        {
            SelectFaction(MBRandom.RandomInt(Factions.Count));
        }

        private void OnFactionSelected(FactionItemVM faction)
        {
            SelectedItem = faction;
            _onSelectionChanged(faction.Faction);
            SelectedFactionName = SelectedItem.Faction.Name.ToString();
        }
    }
}