﻿using System.Collections.Generic;
using System.Linq;
using TaleWorlds.Core.ViewModelCollection;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle.SelectionItem;

namespace KoA.CustomBattle.UI.Components
{
    public class KoAMapSelectionGroupVM : ViewModel
    {
        private string _attackerSiegeMachinesText;
        private string _defenderSiegeMachinesText;
        private bool _isCurrentMapSiege;
        private bool _isSallyOutSelected;
        private SelectorVM<MapItemVM> _mapSelection;
        private string _mapText;
        private string _salloutText;
        private SelectorVM<SceneLevelItemVM> _sceneLevelSelection;
        private string _sceneLevelText;
        private SelectorVM<SeasonItemVM> _seasonSelection;
        private string _seasonText;
        private SelectorVM<TimeOfDayItemVM> _timeOfDaySelection;
        private string _timeOfDayText;
        private string _titleText;
        private SelectorVM<WallHitpointItemVM> _wallHitpointSelection;
        private string _wallHitpointsText;

        public KoAMapSelectionGroupVM()
        {
            _battleMaps = new List<MapItemVM>();
            _villageMaps = new List<MapItemVM>();
            _siegeMaps = new List<MapItemVM>();
            _availableMaps = _battleMaps;
            MapSelection = new SelectorVM<MapItemVM>(0, OnMapSelection);
            WallHitpointSelection = new SelectorVM<WallHitpointItemVM>(0, OnWallHitpointSelection);
            SceneLevelSelection = new SelectorVM<SceneLevelItemVM>(0, OnSceneLevelSelection);
            SeasonSelection = new SelectorVM<SeasonItemVM>(0, OnSeasonSelection);
            TimeOfDaySelection = new SelectorVM<TimeOfDayItemVM>(0, OnTimeOfDaySelection);
            RefreshValues();
        }

        public int SelectedWallBreachedCount { get; private set; }

        public int SelectedSceneLevel { get; private set; }

        public int SelectedTimeOfDay { get; private set; }

        public string SelectedSeasonId { get; private set; }

        public MapItemVM SelectedMap { get; private set; }

        private List<MapItemVM> _battleMaps { get; }

        private List<MapItemVM> _villageMaps { get; }

        private List<MapItemVM> _siegeMaps { get; }

        private List<MapItemVM> _availableMaps { get; set; }

        [DataSourceProperty]
        public SelectorVM<MapItemVM> MapSelection
        {
            get => _mapSelection;
            set
            {
                if (value == _mapSelection)
                    return;
                _mapSelection = value;
                OnPropertyChangedWithValue(value, nameof(MapSelection));
            }
        }

        [DataSourceProperty]
        public SelectorVM<SceneLevelItemVM> SceneLevelSelection
        {
            get => _sceneLevelSelection;
            set
            {
                if (value == _sceneLevelSelection)
                    return;
                _sceneLevelSelection = value;
                OnPropertyChangedWithValue(value, nameof(SceneLevelSelection));
            }
        }

        [DataSourceProperty]
        public SelectorVM<WallHitpointItemVM> WallHitpointSelection
        {
            get => _wallHitpointSelection;
            set
            {
                if (value == _wallHitpointSelection)
                    return;
                _wallHitpointSelection = value;
                OnPropertyChangedWithValue(value, nameof(WallHitpointSelection));
            }
        }

        [DataSourceProperty]
        public SelectorVM<SeasonItemVM> SeasonSelection
        {
            get => _seasonSelection;
            set
            {
                if (value == _seasonSelection)
                    return;
                _seasonSelection = value;
                OnPropertyChangedWithValue(value, nameof(SeasonSelection));
            }
        }

        [DataSourceProperty]
        public SelectorVM<TimeOfDayItemVM> TimeOfDaySelection
        {
            get => _timeOfDaySelection;
            set
            {
                if (value == _timeOfDaySelection)
                    return;
                _timeOfDaySelection = value;
                OnPropertyChangedWithValue(value, nameof(TimeOfDaySelection));
            }
        }

        [DataSourceProperty]
        public bool IsCurrentMapSiege
        {
            get => _isCurrentMapSiege;
            set
            {
                if (value == _isCurrentMapSiege)
                    return;
                _isCurrentMapSiege = value;
                OnPropertyChangedWithValue(value, nameof(IsCurrentMapSiege));
            }
        }

        [DataSourceProperty]
        public bool IsSallyOutSelected
        {
            get => _isSallyOutSelected;
            set
            {
                if (value == _isSallyOutSelected)
                    return;
                _isSallyOutSelected = value;
                OnPropertyChangedWithValue(value, nameof(IsSallyOutSelected));
            }
        }

        [DataSourceProperty]
        public string TitleText
        {
            get => _titleText;
            set
            {
                if (!(value != _titleText))
                    return;
                _titleText = value;
                OnPropertyChangedWithValue(value, nameof(TitleText));
            }
        }

        [DataSourceProperty]
        public string MapText
        {
            get => _mapText;
            set
            {
                if (!(value != _mapText))
                    return;
                _mapText = value;
                OnPropertyChangedWithValue(value, nameof(MapText));
            }
        }

        [DataSourceProperty]
        public string SeasonText
        {
            get => _seasonText;
            set
            {
                if (!(value != _seasonText))
                    return;
                _seasonText = value;
                OnPropertyChangedWithValue(value, nameof(SeasonText));
            }
        }

        [DataSourceProperty]
        public string TimeOfDayText
        {
            get => _timeOfDayText;
            set
            {
                if (!(value != _timeOfDayText))
                    return;
                _timeOfDayText = value;
                OnPropertyChangedWithValue(value, nameof(TimeOfDayText));
            }
        }

        [DataSourceProperty]
        public string SceneLevelText
        {
            get => _sceneLevelText;
            set
            {
                if (!(value != _sceneLevelText))
                    return;
                _sceneLevelText = value;
                OnPropertyChangedWithValue(value, nameof(SceneLevelText));
            }
        }

        [DataSourceProperty]
        public string WallHitpointsText
        {
            get => _wallHitpointsText;
            set
            {
                if (!(value != _wallHitpointsText))
                    return;
                _wallHitpointsText = value;
                OnPropertyChangedWithValue(value, nameof(WallHitpointsText));
            }
        }

        [DataSourceProperty]
        public string AttackerSiegeMachinesText
        {
            get => _attackerSiegeMachinesText;
            set
            {
                if (!(value != _attackerSiegeMachinesText))
                    return;
                _attackerSiegeMachinesText = value;
                OnPropertyChangedWithValue(value, nameof(AttackerSiegeMachinesText));
            }
        }

        [DataSourceProperty]
        public string DefenderSiegeMachinesText
        {
            get => _defenderSiegeMachinesText;
            set
            {
                if (!(value != _defenderSiegeMachinesText))
                    return;
                _defenderSiegeMachinesText = value;
                OnPropertyChangedWithValue(value, nameof(DefenderSiegeMachinesText));
            }
        }

        [DataSourceProperty]
        public string SalloutText
        {
            get => _salloutText;
            set
            {
                if (!(value != _salloutText))
                    return;
                _salloutText = value;
                OnPropertyChangedWithValue(value, nameof(SalloutText));
            }
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            PrepareMapLists();
            TitleText = new TextObject("{=w9m11T1y}Map").ToString();
            MapText = new TextObject("{=w9m11T1y}Map").ToString();
            SeasonText = new TextObject("{=xTzDM5XE}Season").ToString();
            TimeOfDayText = new TextObject("{=DszSWnc3}Time of Day").ToString();
            SceneLevelText = new TextObject("{=0s52GQJt}Scene Level").ToString();
            WallHitpointsText = new TextObject("{=4IuXGSdc}Wall Hitpoints").ToString();
            AttackerSiegeMachinesText = new TextObject("{=AmfIfeIc}Choose Attacker Siege Machines").ToString();
            DefenderSiegeMachinesText = new TextObject("{=UoiSWe87}Choose Defender Siege Machines").ToString();
            SalloutText = new TextObject("{=EcKMGoFv}Sallyout").ToString();
            MapSelection.ItemList.Clear();
            WallHitpointSelection.ItemList.Clear();
            SceneLevelSelection.ItemList.Clear();
            SeasonSelection.ItemList.Clear();
            TimeOfDaySelection.ItemList.Clear();
            foreach (var availableMap in _availableMaps)
                MapSelection.AddItem(new MapItemVM(availableMap.MapName, availableMap.MapId));
            foreach (var wallHitpoint in CustomBattleData.WallHitpoints)
                WallHitpointSelection.AddItem(new WallHitpointItemVM(wallHitpoint.Item1, wallHitpoint.Item2));
            foreach (var sceneLevel in CustomBattleData.SceneLevels)
                SceneLevelSelection.AddItem(new SceneLevelItemVM(sceneLevel));
            foreach (var season in CustomBattleData.Seasons)
                SeasonSelection.AddItem(new SeasonItemVM(season.Item1, season.Item2));
            foreach (var tuple in CustomBattleData.TimesOfDay)
                TimeOfDaySelection.AddItem(new TimeOfDayItemVM(tuple.Item1, (int)tuple.Item2));
            MapSelection.SelectedIndex = 0;
            WallHitpointSelection.SelectedIndex = 0;
            SceneLevelSelection.SelectedIndex = 0;
            SeasonSelection.SelectedIndex = 0;
            TimeOfDaySelection.SelectedIndex = 0;
        }

        public void ExecuteSallyOutChange()
        {
            IsSallyOutSelected = !IsSallyOutSelected;
        }

        private void PrepareMapLists()
        {
            _battleMaps.Clear();
            _villageMaps.Clear();
            _siegeMaps.Clear();
            foreach (var customBattleSceneData in KoACustomBattle.Current.CustomBattleScenes.ToList())
            {
                var mapItemVm = new MapItemVM(customBattleSceneData.Name.ToString(), customBattleSceneData.SceneID);
                if (customBattleSceneData.IsVillageMap)
                    _villageMaps.Add(mapItemVm);
                else if (customBattleSceneData.IsSiegeMap)
                    _siegeMaps.Add(mapItemVm);
                else if (!customBattleSceneData.IsLordsHallMap)
                    _battleMaps.Add(mapItemVm);
            }

            var comparer = Comparer<MapItemVM>.Create((x, y) => x.MapName.CompareTo(y.MapName));
            _battleMaps.Sort(comparer);
            _villageMaps.Sort(comparer);
            _siegeMaps.Sort(comparer);
        }

        private void OnMapSelection(SelectorVM<MapItemVM> selector)
        {
            SelectedMap = selector.SelectedItem;
        }

        private void OnWallHitpointSelection(SelectorVM<WallHitpointItemVM> selector)
        {
            SelectedWallBreachedCount = selector.SelectedItem.BreachedWallCount;
        }

        private void OnSceneLevelSelection(SelectorVM<SceneLevelItemVM> selector)
        {
            SelectedSceneLevel = selector.SelectedItem.Level;
        }

        private void OnSeasonSelection(SelectorVM<SeasonItemVM> selector)
        {
            SelectedSeasonId = selector.SelectedItem.SeasonId;
        }

        private void OnTimeOfDaySelection(SelectorVM<TimeOfDayItemVM> selector)
        {
            SelectedTimeOfDay = selector.SelectedItem.TimeOfDay;
        }

        public void OnGameTypeChange(CustomBattleGameType gameType)
        {
            MapSelection.ItemList.Clear();
            switch (gameType)
            {
                case CustomBattleGameType.Battle:
                    IsCurrentMapSiege = false;
                    _availableMaps = _battleMaps;
                    break;
                case CustomBattleGameType.Village:
                    IsCurrentMapSiege = false;
                    _availableMaps = _villageMaps;
                    break;
                case CustomBattleGameType.Siege:
                    IsCurrentMapSiege = true;
                    _availableMaps = _siegeMaps;
                    break;
            }

            foreach (var availableMap in _availableMaps)
                MapSelection.AddItem(availableMap);
            MapSelection.SelectedIndex = 0;
        }

        public void RandomizeAll()
        {
            MapSelection.ExecuteRandomize();
            SceneLevelSelection.ExecuteRandomize();
            SeasonSelection.ExecuteRandomize();
            WallHitpointSelection.ExecuteRandomize();
            TimeOfDaySelection.ExecuteRandomize();
        }
    }
}