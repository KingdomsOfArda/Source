﻿using System.Collections.Generic;
using System.Linq;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.CustomBattle;
using TaleWorlds.ObjectSystem;

namespace KoA.CustomBattle
{
    public class KoACustomBattleState : GameState
    {
        public override bool IsMusicMenuState => true;

        public CustomBattleCombatant[] GetCustomBattleParties(
            BasicCharacterObject playerCharacter,
            BasicCharacterObject playerSideGeneralCharacter,
            BasicCharacterObject enemyCharacter,
            BasicCultureObject playerFaction,
            int[] playerNumbers,
            List<BasicCharacterObject>[] playerTroopSelections,
            BasicCultureObject enemyFaction,
            int[] enemyNumbers,
            List<BasicCharacterObject>[] enemyTroopSelections,
            bool isPlayerAttacker)
        {
            var customBattleCombatantArray = new CustomBattleCombatant[2]
            {
                new(new TextObject("{=sSJSTe5p}Player Party"), playerFaction, Banner.CreateRandomBanner()),
                new(new TextObject("{=0xC75dN6}Enemy Party"), enemyFaction, Banner.CreateRandomBanner())
            };

            customBattleCombatantArray[0].Side = isPlayerAttacker ? BattleSideEnum.Attacker : BattleSideEnum.Defender;
            customBattleCombatantArray[0].AddCharacter(playerCharacter, 1);

            if (playerSideGeneralCharacter != null)
            {
                customBattleCombatantArray[0].AddCharacter(playerSideGeneralCharacter, 1);
                customBattleCombatantArray[0].SetGeneral(playerSideGeneralCharacter);
            }
            else
            {
                customBattleCombatantArray[0].SetGeneral(playerCharacter);
            }

            customBattleCombatantArray[1].Side = customBattleCombatantArray[0].Side.GetOppositeSide();
            customBattleCombatantArray[1].AddCharacter(enemyCharacter, 1);

            for (var index = 0; index < customBattleCombatantArray.Length; ++index)
                PopulateListsWithDefaults(ref customBattleCombatantArray[index],
                    index == 0 ? playerNumbers : enemyNumbers,
                    index == 0 ? playerTroopSelections : enemyTroopSelections);

            return customBattleCombatantArray;
        }

        private void PopulateListsWithDefaults(
            ref CustomBattleCombatant customBattleParties,
            int[] numbers,
            List<BasicCharacterObject>[] troopList)
        {
            var basicCulture = customBattleParties.BasicCulture;
            troopList ??= new List<BasicCharacterObject>[4]
            {
                new(),
                new(),
                new(),
                new()
            };

            if (troopList[0].Count == 0)
                troopList[0] = new List<BasicCharacterObject>
                {
                    Helper.GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.Infantry)
                };

            if (troopList[1].Count == 0)
                troopList[1] = new List<BasicCharacterObject>
                {
                    Helper.GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.Ranged)
                };

            if (troopList[2].Count == 0)
                troopList[2] = new List<BasicCharacterObject>
                {
                    Helper.GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.Cavalry)
                };

            if (troopList[3].Count == 0)
                troopList[3] = new List<BasicCharacterObject>
                {
                    Helper.GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.HorseArcher)
                };

            if (!troopList[3].Any() || troopList[3].All(troop => troop == null))
            {
                numbers[2] += numbers[3] / 3;
                numbers[1] += numbers[3] / 3;
                numbers[0] += numbers[3] / 3;
                numbers[0] += numbers[3] - numbers[3] / 3 * 3;
                numbers[3] = 0;
            }

            for (var index1 = 0; index1 < 4; ++index1)
            {
                var count = troopList[index1].Count;
                var number1 = numbers[index1];
                if (number1 > 0)
                {
                    var num1 = number1 / (float)count;
                    var num2 = 0.0f;
                    for (var index2 = 0; index2 < count; ++index2)
                    {
                        var num3 = num1 + (double)num2;
                        var number2 = MathF.Floor((float)num3);
                        num2 = (float)num3 - number2;
                        customBattleParties.AddCharacter(troopList[index1][index2], number2);
                        numbers[index1] -= number2;
                        if (index2 == count - 1 && numbers[index1] > 0)
                        {
                            customBattleParties.AddCharacter(troopList[index1][index2], numbers[index1]);
                            numbers[index1] = 0;
                        }
                    }
                }
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            Helper.AssertMissingTroopsForDebug();
        }

        public static string EnableRecordMission(List<string> strings)
        {
            if (!(GameStateManager.Current.ActiveState is CustomBattleState))
                return "Mission recording for custom battle can only be enabled while in custom battle screen.";
            MissionState.RecordMission = true;
            return "Mission recording activated.";
        }

        public static class Helper
        {
            public static void AssertMissingTroopsForDebug()
            {
                foreach (var objectType in MBObjectManager.Instance.GetObjectTypeList<BasicCultureObject>())
                    for (var index = 0; index < 4; ++index)
                        GetDefaultTroopOfFormationForFaction(objectType, (FormationClass)index);
            }

            public static BasicCharacterObject GetDefaultTroopOfFormationForFaction(BasicCultureObject culture,
                FormationClass formation)
            {
                if (culture.StringId.ToLower() == "empire")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("imperial_veteran_infantryman");
                        case FormationClass.Ranged:
                            return GetTroopFromId("imperial_archer");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("imperial_heavy_horseman");
                        case FormationClass.HorseArcher:
                            return GetTroopFromId("bucellarii");
                    }
                else if (culture.StringId.ToLower() == "sturgia")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("sturgian_spearman");
                        case FormationClass.Ranged:
                            return GetTroopFromId("sturgian_archer");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("sturgian_hardened_brigand");
                    }
                else if (culture.StringId.ToLower() == "aserai")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("aserai_infantry");
                        case FormationClass.Ranged:
                            return GetTroopFromId("aserai_archer");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("aserai_mameluke_cavalry");
                        case FormationClass.HorseArcher:
                            return GetTroopFromId("aserai_faris");
                    }
                else if (culture.StringId.ToLower() == "vlandia")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("vlandian_swordsman");
                        case FormationClass.Ranged:
                            return GetTroopFromId("vlandian_hardened_crossbowman");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("vlandian_knight");
                    }
                else if (culture.StringId.ToLower() == "battania")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("battanian_picked_warrior");
                        case FormationClass.Ranged:
                            return GetTroopFromId("battanian_hero");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("battanian_scout");
                    }
                else if (culture.StringId.ToLower() == "khuzait")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("khuzait_spear_infantry");
                        case FormationClass.Ranged:
                            return GetTroopFromId("khuzait_archer");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("khuzait_lancer");
                        case FormationClass.HorseArcher:
                            return GetTroopFromId("khuzait_horse_archer");
                    }
                else if (culture.StringId.ToLower() == "gondor")
                    switch (formation)
                    {
                        case FormationClass.Infantry:
                            return GetTroopFromId("gondor_veteran");
                        case FormationClass.Ranged:
                            return GetTroopFromId("gondor_archer");
                        case FormationClass.Cavalry:
                            return GetTroopFromId("gondor_veteran");
                        case FormationClass.HorseArcher:
                            return GetTroopFromId("gondor_veteran");
                    }

                return null;
            }

            private static BasicCharacterObject GetTroopFromId(string troopId)
            {
                return MBObjectManager.Instance.GetObject<BasicCharacterObject>(troopId);
            }
        }
    }
}