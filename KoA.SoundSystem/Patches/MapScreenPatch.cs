﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HarmonyLib;
using SandBox.View;
using SandBox.View.Map;
using TaleWorlds.CampaignSystem;
using TaleWorlds.MountAndBlade;

namespace KoA.SoundSystem.Patches
{
    [HarmonyPatch(typeof(MapScreen))]
    [HarmonyPatch("OnFinalize")]
    public class MapScreenPatch_OnFinalize
    {
        public static MethodInfo m_current = typeof(MBMusicManager)
            .GetProperty("Current", BindingFlags.Static | BindingFlags.Public).GetMethod;

        private static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);
            for (var i = 0; i < codes.Count; i++)
                if (codes[i].Calls(m_current) && codes[i + 2].Calls(m_current))
                {
                    codes.RemoveRange(i, 4);
                    break;
                }

            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(MapScreen))]
    [HarmonyPatch(MethodType.Constructor)]
    [HarmonyPatch(new[] { typeof(MapState) })]
    public class MapScreenPatch_Constructor
    {
        public static MethodInfo m_create =
            typeof(CampaignMusicHandler).GetMethod("Create", BindingFlags.Static | BindingFlags.Public);

        private static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);
            for (var i = 0; i < codes.Count; i++)
                if (codes[i].Calls(m_create))
                {
                    codes.RemoveRange(i, 1);
                    break;
                }

            return codes.AsEnumerable();
        }

        private static void Postfix()
        {
            Music.CampaignMusicHandler.Create();
        }
    }
}