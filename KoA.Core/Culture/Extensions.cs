﻿using System;
using TaleWorlds.Core;

namespace KoA.Core.Culture
{
    public static class Extensions
    {
        public static KoACultureCode GetKoACultureCode(this BasicCultureObject cultureObject)
        {
            return Enum.TryParse<KoACultureCode>(cultureObject.StringId, true, out var result)
                ? result
                : KoACultureCode.Invalid;
        }
    }
}