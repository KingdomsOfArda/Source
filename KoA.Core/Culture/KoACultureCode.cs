﻿namespace KoA.Core.Culture
{
    public enum KoACultureCode
    {
        Invalid,
        Gondor,
        Rohan,
        Mordor,
        Isengard,
        Harad,
        Dunland
    }
}