﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;

namespace KoA.Core.Missions.Behaviours
{
    public class ArmourBehavior : MissionBehavior
    {
        private static Dictionary<string, int> _armourHitpoints;
        public override MissionBehaviorType BehaviorType { get; }  = MissionBehaviorType.Other;

        public override void EarlyStart()
        {
            if (_armourHitpoints != null) return;
            _armourHitpoints = new Dictionary<string, int>();
            LoadAllArmourHitpoints();
        }

        public override void OnAgentBuild(Agent agent, Banner banner)
        {
            if (agent.IsHuman)
            {
                var hitpoints = GetArmourHitpoints(agent);
                var healthRatio = agent.Health / agent.HealthLimit;
                agent.HealthLimit += hitpoints;
                agent.Health = agent.HealthLimit * healthRatio;
            }
        }



        public int GetArmourHitpoints(Agent agent)
        {
            var hitpoints = 0;
            for (var equipmentIndex = EquipmentIndex.NumAllArmorSlots;
                equipmentIndex < EquipmentIndex.ArmorItemEndSlot;
                equipmentIndex++)
            {
                var equipmentElement = agent.SpawnEquipment[equipmentIndex];
                if (equipmentElement.Item != null && _armourHitpoints.ContainsKey(equipmentElement.Item.StringId))
                    hitpoints += _armourHitpoints[equipmentElement.Item.StringId];
            }

            return hitpoints;
        }

        private XDocument LoadXmlFile(string path)
        {
            var xml = new StreamReader(path).ReadToEnd();
            return XDocument.Parse(xml);
        }

        private void LoadAllArmourHitpoints()
        {
            foreach (var filePath in Directory.GetFiles(BasePath.Name + "Modules/KingdomsOfArda/ModuleData/items",
                "*.xml")) LoadArmourHitpoints(filePath);
        }

        private void LoadArmourHitpoints(string path)
        {
            // Bannerlord uses the spelling "Armor" instead of "Armour"
            var xDocument = LoadXmlFile(path);
            foreach (var item in xDocument.Descendants("Item")
                .Where(item => item.Descendants("ItemComponent").Elements("Armor").Count() == 1))
            {
                var armour = item.Descendants("ItemComponent").Elements("Armor").FirstOrDefault();
                var id = item.Attribute("id")?.Value;
                var hitpoints = armour?.Attribute("hitpoints")?.Value;
                if (id == null || !int.TryParse(hitpoints, out var value)) continue;
                _armourHitpoints.Add(id, value);
            }
        }
    }
}