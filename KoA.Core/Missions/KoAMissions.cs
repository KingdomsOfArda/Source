﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.Missions.Handlers;
using TaleWorlds.MountAndBlade.MissionSpawnHandlers;
using TaleWorlds.MountAndBlade.Source.Missions;
using TaleWorlds.MountAndBlade.Source.Missions.Handlers;
using TaleWorlds.MountAndBlade.Source.Missions.Handlers.Logic;

namespace KoA.Core.Missions
{
    [MissionManager]
    public static class KoAMissions
    {
        private const string Level1Tag = "level_1";
        private const string Level2Tag = "level_2";
        private const string Level3Tag = "level_3";
        private const string SiegeTag = "siege";
        private const string SallyOutTag = "sally";

        private static Type GetSiegeWeaponType(SiegeEngineType siegeWeaponType)
        {
            if (siegeWeaponType == DefaultSiegeEngineTypes.Ladder)
                return typeof(SiegeLadder);
            if (siegeWeaponType == DefaultSiegeEngineTypes.Ballista)
                return typeof(Ballista);
            if (siegeWeaponType == DefaultSiegeEngineTypes.FireBallista)
                return typeof(FireBallista);
            if (siegeWeaponType == DefaultSiegeEngineTypes.Ram ||
                siegeWeaponType == DefaultSiegeEngineTypes.ImprovedRam)
                return typeof(BatteringRam);
            if (siegeWeaponType == DefaultSiegeEngineTypes.SiegeTower ||
                siegeWeaponType == DefaultSiegeEngineTypes.HeavySiegeTower)
                return typeof(SiegeTower);
            if (siegeWeaponType == DefaultSiegeEngineTypes.Onager ||
                siegeWeaponType == DefaultSiegeEngineTypes.Catapult)
                return typeof(Mangonel);
            if (siegeWeaponType == DefaultSiegeEngineTypes.FireOnager ||
                siegeWeaponType == DefaultSiegeEngineTypes.FireCatapult)
                return typeof(FireMangonel);
            return siegeWeaponType == DefaultSiegeEngineTypes.Trebuchet ||
                   siegeWeaponType == DefaultSiegeEngineTypes.Bricole
                ? typeof(Trebuchet)
                : null;
        }

        private static Dictionary<Type, int> GetSiegeWeaponTypes(
            Dictionary<SiegeEngineType, int> values)
        {
            var siegeWeaponTypes = new Dictionary<Type, int>();
            foreach (var keyValuePair in values)
                siegeWeaponTypes.Add(GetSiegeWeaponType(keyValuePair.Key), keyValuePair.Value);
            return siegeWeaponTypes;
        }

        private static AtmosphereInfo CreateAtmosphereInfoForMission(
            string seasonId,
            int timeOfDay)
        {
            if (seasonId == "default") return new AtmosphereInfo { AtmosphereName = "" };

            var dictionary1 = new Dictionary<string, int>();
            dictionary1.Add("spring", 0);
            dictionary1.Add("summer", 1);
            dictionary1.Add("fall", 2);
            dictionary1.Add("winter", 3);
            var num = 0;
            dictionary1.TryGetValue(seasonId, out num);
            var dictionary2 = new Dictionary<int, string>();
            dictionary2.Add(6, "TOD_06_00_SemiCloudy");
            dictionary2.Add(12, "TOD_12_00_SemiCloudy");
            dictionary2.Add(15, "TOD_04_00_SemiCloudy");
            dictionary2.Add(18, "TOD_03_00_SemiCloudy");
            dictionary2.Add(22, "TOD_01_00_SemiCloudy");
            var str = "field_battle";
            dictionary2.TryGetValue(timeOfDay, out str);
            return new AtmosphereInfo
            {
                AtmosphereName = str,
                TimeInfo = new TimeInformation { Season = num }
            };
        }

        [MissionMethod]
        public static Mission OpenCustomBattleMission(
            string scene,
            BasicCharacterObject character,
            CustomBattleCombatant playerParty,
            CustomBattleCombatant enemyParty,
            bool isPlayerGeneral,
            BasicCharacterObject playerSideGeneralCharacter,
            string sceneLevels = "",
            string seasonString = "",
            float timeOfDay = 6f)
        {
            var playerSide = playerParty.Side;
            var isPlayerAttacker = playerSide == BattleSideEnum.Attacker;
            var troopSuppliers = new IMissionTroopSupplier[2];
            var battleTroopSupplier1 = new CustomBattleTroopSupplier(playerParty, true);
            troopSuppliers[(int)playerParty.Side] = battleTroopSupplier1;
            var battleTroopSupplier2 = new CustomBattleTroopSupplier(enemyParty, false);
            troopSuppliers[(int)enemyParty.Side] = battleTroopSupplier2;
            var isPlayerSergeant = !isPlayerGeneral;
            return MissionState.OpenNew("CustomBattle", new MissionInitializerRecord(scene)
            {
                DoNotUseLoadingScreen = false,
                PlayingInCampaignMode = false,
                AtmosphereOnCampaign = CreateAtmosphereInfoForMission(seasonString, (int)timeOfDay),
                SceneLevels = sceneLevels,
                TimeOfDay = timeOfDay
            }, missionController =>
                new MissionBehavior[22]
                {
                    new MissionAgentSpawnLogic(troopSuppliers, playerSide),
                    new CustomBattleAgentLogic(),
                    new CustomBattleMissionSpawnHandler(!isPlayerAttacker ? playerParty : enemyParty,
                        isPlayerAttacker ? playerParty : enemyParty),
                    new MissionOptionsComponent(),
                    new BattleEndLogic(),
                    new MissionCombatantsLogic(null,
                        playerParty,
                        !isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                        isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                        Mission.MissionTeamAITypeEnum.FieldBattle, isPlayerSergeant),
                    new BattleObserverMissionLogic(),
                    new AgentHumanAILogic(),
                    new AgentVictoryLogic(),
                    new MissionAgentPanicHandler(),
                    new BattleMissionAgentInteractionLogic(),
                    new AgentMoraleInteractionLogic(),
                    new AssignPlayerRoleInTeamMissionController(isPlayerGeneral, isPlayerSergeant,
                        false,
                        isPlayerSergeant
                            ? Enumerable.Repeat(character.StringId, 1).ToList()
                            : new List<string>()),
                    new CreateBodyguardMissionBehavior(
                        isPlayerAttacker & isPlayerGeneral
                            ? character.GetName()
                            : isPlayerAttacker & isPlayerSergeant
                                ? playerSideGeneralCharacter.GetName()
                                : null,
                        !isPlayerAttacker & isPlayerGeneral
                            ? character.GetName()
                            : !isPlayerAttacker & isPlayerSergeant
                                ? playerSideGeneralCharacter.GetName()
                                : null),
                    new EquipmentControllerLeaveLogic(),
                    new MissionHardBorderPlacer(),
                    new MissionBoundaryPlacer(),
                    new MissionBoundaryCrossingHandler(),
                    new HighlightsController(),
                    new BattleHighlightsController(),
                    new DeploymentMissionController(isPlayerAttacker),
                    new BattleDeploymentHandler(isPlayerAttacker)
                });
        }

        [MissionMethod]
        public static Mission OpenSiegeMissionWithDeployment(
            string scene,
            BasicCharacterObject character,
            CustomBattleCombatant playerParty,
            CustomBattleCombatant enemyParty,
            bool isPlayerGeneral,
            float[] wallHitPointPercentages,
            bool hasAnySiegeTower,
            Dictionary<SiegeEngineType, int> siegeWeaponsCountOfAttackers,
            Dictionary<SiegeEngineType, int> siegeWeaponsCountOfDefenders,
            bool isPlayerAttacker,
            int sceneUpgradeLevel = 0,
            string seasonString = "",
            bool isSallyOut = false,
            bool isReliefForceAttack = false,
            float timeOfDay = 6f)
        {
            string str1;
            switch (sceneUpgradeLevel)
            {
                case 1:
                    str1 = "level_1";
                    break;
                case 2:
                    str1 = "level_2";
                    break;
                default:
                    str1 = "level_3";
                    break;
            }

            var str2 = str1 + " siege";
            var playerSide = playerParty.Side;
            var troopSuppliers = new IMissionTroopSupplier[2];
            var battleTroopSupplier1 = new CustomBattleTroopSupplier(playerParty, true);
            troopSuppliers[(int)playerParty.Side] = battleTroopSupplier1;
            var battleTroopSupplier2 = new CustomBattleTroopSupplier(enemyParty, false);
            troopSuppliers[(int)enemyParty.Side] = battleTroopSupplier2;
            var isPlayerSergeant = !isPlayerGeneral;
            return MissionState.OpenNew("CustomSiegeBattle", new MissionInitializerRecord(scene)
            {
                PlayingInCampaignMode = false,
                AtmosphereOnCampaign = CreateAtmosphereInfoForMission(seasonString, (int)timeOfDay),
                SceneLevels = str2,
                TimeOfDay = timeOfDay
            }, mission =>
            {
                var missionBehaviorList = new List<MissionBehavior>();
                missionBehaviorList.Add(new BattleSpawnLogic(isSallyOut
                    ? "sally_out_set"
                    : isReliefForceAttack
                        ? "relief_force_attack_set"
                        : "battle_set"));
                missionBehaviorList.Add(new MissionOptionsComponent());
                missionBehaviorList.Add(new BattleEndLogic());
                missionBehaviorList.Add(new MissionCombatantsLogic(null,
                    playerParty,
                    !isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                    isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                    !isSallyOut ? Mission.MissionTeamAITypeEnum.Siege : Mission.MissionTeamAITypeEnum.SallyOut,
                    isPlayerSergeant));
                missionBehaviorList.Add(new SiegeMissionPreparationHandler(isSallyOut,
                    isReliefForceAttack, wallHitPointPercentages, hasAnySiegeTower));
                missionBehaviorList.Add(new MissionAgentSpawnLogic(troopSuppliers, playerSide, true));
                if (isSallyOut)
                    missionBehaviorList.Add(new CustomSiegeSallyOutMissionSpawnHandler(
                        !isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                        isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty));
                else
                    missionBehaviorList.Add(new CustomSiegeMissionSpawnHandler(
                        !isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                        isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty, false));
                missionBehaviorList.Add(new BattleObserverMissionLogic());
                missionBehaviorList.Add(new CustomBattleAgentLogic());
                missionBehaviorList.Add(new AgentHumanAILogic());
                if (!isSallyOut)
                    missionBehaviorList.Add(new AmmoSupplyLogic(new List<BattleSideEnum>
                    {
                        BattleSideEnum.Defender
                    }));
                missionBehaviorList.Add(new AgentVictoryLogic());
                missionBehaviorList.Add(
                    new AssignPlayerRoleInTeamMissionController(isPlayerGeneral, isPlayerSergeant,
                        false));
                missionBehaviorList.Add(new CreateBodyguardMissionBehavior(
                    isPlayerAttacker & isPlayerGeneral ? character.GetName() : null, null,
                    createBodyguard: false));
                missionBehaviorList.Add(new MissionAgentPanicHandler());
                missionBehaviorList.Add(new MissionBoundaryPlacer());
                missionBehaviorList.Add(new MissionBoundaryCrossingHandler());
                missionBehaviorList.Add(new AgentMoraleInteractionLogic());
                missionBehaviorList.Add(new HighlightsController());
                missionBehaviorList.Add(new BattleHighlightsController());
                missionBehaviorList.Add(new EquipmentControllerLeaveLogic());
                missionBehaviorList.Add(new SiegeMissionController(
                    GetSiegeWeaponTypes(siegeWeaponsCountOfAttackers),
                    GetSiegeWeaponTypes(siegeWeaponsCountOfDefenders), isPlayerAttacker, isSallyOut));
                missionBehaviorList.Add(new SiegeDeploymentHandler(isPlayerAttacker,
                    isPlayerAttacker
                        ? GetSiegeWeaponTypes(siegeWeaponsCountOfAttackers)
                        : GetSiegeWeaponTypes(siegeWeaponsCountOfDefenders)));
                return missionBehaviorList.ToArray();
            });
        }

        [MissionMethod]
        public static Mission OpenCustomBattleLordsHallMission(
            string scene,
            BasicCharacterObject character,
            CustomBattleCombatant playerParty,
            CustomBattleCombatant enemyParty,
            BasicCharacterObject playerSideGeneralCharacter,
            string sceneLevels = "",
            int sceneUpgradeLevel = 0,
            string seasonString = "",
            float timeOfDay = 6f)
        {
            var limit = MathF.Round(18.9f);
            var playerSide = BattleSideEnum.Attacker;
            var isPlayerAttacker = playerSide == BattleSideEnum.Attacker;
            var troopSuppliers = new IMissionTroopSupplier[2];
            var battleTroopSupplier1 = new CustomBattleTroopSupplier(playerParty, true);
            troopSuppliers[(int)playerParty.Side] = battleTroopSupplier1;
            var limits = new CustomBattleTroopSupplier.Limits();
            limits.AddTroopSizeLimitForFormation(FormationClass.Ranged, limit);
            var battleTroopSupplier2 = new CustomBattleTroopSupplier(enemyParty, false, limits);
            troopSuppliers[(int)enemyParty.Side] = battleTroopSupplier2;
            return MissionState.OpenNew("CustomBattleLordsHall", new MissionInitializerRecord(scene)
            {
                DoNotUseLoadingScreen = false,
                PlayingInCampaignMode = false,
                SceneLevels = "siege",
                TimeOfDay = timeOfDay
            }, missionController =>
                new MissionBehavior[16]
                {
                    new MissionOptionsComponent(),
                    new BattleEndLogic(),
                    new MissionCombatantsLogic(null,
                        playerParty,
                        !isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                        isPlayerAttacker ? playerParty : (IBattleCombatant)enemyParty,
                        Mission.MissionTeamAITypeEnum.NoTeamAI, false),
                    new BattleMissionStarterLogic(),
                    new AgentHumanAILogic(),
                    new LordsHallFightMissionController(troopSuppliers, 3f, 0.7f, 19, 27, playerSide),
                    new BattleObserverMissionLogic(),
                    new CustomBattleAgentLogic(),
                    new AgentVictoryLogic(),
                    new AmmoSupplyLogic(new List<BattleSideEnum>
                    {
                        BattleSideEnum.Defender
                    }),
                    new MissionHardBorderPlacer(),
                    new MissionBoundaryPlacer(),
                    new MissionBoundaryCrossingHandler(),
                    new BattleMissionAgentInteractionLogic(),
                    new HighlightsController(),
                    new BattleHighlightsController()
                });
        }

        private enum CustomBattleGameTypes
        {
            AttackerGeneral,
            DefenderGeneral,
            AttackerSergeant,
            DefenderSergeant
        }
    }
}