﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using SandBox;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;
using TaleWorlds.Engine;
using TaleWorlds.Library;
using TaleWorlds.ModuleManager;
using TaleWorlds.MountAndBlade;
using BinaryReader = System.IO.BinaryReader;
using BinaryWriter = System.IO.BinaryWriter;

namespace KoA.Core.EditorScripts
{
    public class KoASettlementPositionScript : ScriptComponentBehavior
    {
        public SimpleButton CheckPositions;
        public SimpleButton ComputeAndSaveSettlementDistanceCache;
        public SimpleButton SavePositions;

        private static string SettlementsXmlPath =>
            ModuleHelper.GetModuleFullPath("KingdomsOfArda") + "ModuleData/settlements.xml";

        private static string SettlementsDistanceCacheFilePath => ModuleHelper.GetModuleFullPath("KingdomsOfArda") +
                                                                  "ModuleData/KOA_settlements_distance_cache.bin";

        protected override void OnEditorVariableChanged(string variableName)
        {
            base.OnEditorVariableChanged(variableName);
            if (variableName == "SavePositions")
                SaveSettlementPositions();
            if (variableName == "ComputeAndSaveSettlementDistanceCache")
                SaveSettlementDistanceCache();
            if (!(variableName == "CheckPositions"))
                return;
            CheckSettlementPositions();
        }

        protected override void OnSceneSave(string saveFolder)
        {
            base.OnSceneSave(saveFolder);
            SaveSettlementPositions();
        }

        private void CheckSettlementPositions()
        {
            var xmlDocument = LoadXmlFile(SettlementsXmlPath);
            GameEntity.RemoveAllChildren();
            foreach (XmlNode selectNode in xmlDocument.DocumentElement.SelectNodes("Settlement"))
            {
                var firstEntityWithName = Scene.GetFirstEntityWithName(selectNode.Attributes["id"].Value);
                var origin = firstEntityWithName.GetGlobalFrame().origin;
                var vec3 = new Vec3();
                var children = new List<GameEntity>();
                firstEntityWithName.GetChildrenRecursive(ref children);
                var flag = false;
                foreach (var gameEntity in children)
                    if (gameEntity.HasTag("main_map_city_gate"))
                    {
                        vec3 = gameEntity.GetGlobalFrame().origin;
                        flag = true;
                        break;
                    }

                var pos = origin;
                if (flag)
                    pos = vec3;
                var record = new PathFaceRecord(-1, -1, -1);
                GameEntity.Scene.GetNavMeshFaceIndex(ref record, pos.AsVec2, true);
                var num = 0;
                if (record.IsValid())
                    num = record.FaceGroupIndex;
                if (num == 0 || num == 7 || num == 8 || num == 10 || num == 11 || num == 13 || num == 14)
                {
                    MBEditor.ZoomToPosition(pos);
                    break;
                }
            }
        }

        protected override void OnInit()
        {
            try
            {
                Debug.Print("SettlementsDistanceCacheFilePath: " +
                            SettlementsDistanceCacheFilePath);
                var reader = new BinaryReader(File.Open(SettlementsDistanceCacheFilePath,
                    FileMode.Open, FileAccess.Read));
                if (Campaign.Current.Models.MapDistanceModel is DefaultMapDistanceModel)
                    ((DefaultMapDistanceModel)Campaign.Current.Models.MapDistanceModel).LoadCacheFromFile(reader);
                reader.Close();
            }
            catch
            {
                Debug.Print(
                    "SettlementsDistanceCacheFilePath could not be read!. Campaign performance will be affected very badly.");
            }
        }

        private List<SettlementRecord> LoadSettlementData(
            XmlDocument settlementDocument)
        {
            var settlementRecordList = new List<SettlementRecord>();
            GameEntity.RemoveAllChildren();
            foreach (XmlNode selectNode in settlementDocument.DocumentElement.SelectNodes("Settlement"))
            {
                var settlementName = selectNode.Attributes["name"].Value;
                var str = selectNode.Attributes["id"].Value;
                var firstEntityWithName = Scene.GetFirstEntityWithName(str);
                var asVec2 = firstEntityWithName.GetGlobalFrame().origin.AsVec2;
                var vec2 = new Vec2();
                var children = new List<GameEntity>();
                firstEntityWithName.GetChildrenRecursive(ref children);
                var hasGate = false;
                foreach (var gameEntity in children)
                    if (gameEntity.HasTag("main_map_city_gate"))
                    {
                        vec2 = gameEntity.GetGlobalFrame().origin.AsVec2;
                        hasGate = true;
                    }

                settlementRecordList.Add(new SettlementRecord(settlementName, str, asVec2,
                    hasGate ? vec2 : asVec2, selectNode, hasGate));
            }

            return settlementRecordList;
        }

        private void SaveSettlementPositions()
        {
            var settlementDocument = LoadXmlFile(SettlementsXmlPath);
            foreach (var settlementRecord in LoadSettlementData(settlementDocument))
            {
                if (settlementRecord.Node.Attributes["posX"] == null)
                {
                    var attribute = settlementDocument.CreateAttribute("posX");
                    settlementRecord.Node.Attributes.Append(attribute);
                }

                var attribute1 = settlementRecord.Node.Attributes["posX"];
                var vec2 = settlementRecord.Position;
                var str1 = vec2.X.ToString();
                attribute1.Value = str1;
                if (settlementRecord.Node.Attributes["posY"] == null)
                {
                    var attribute2 = settlementDocument.CreateAttribute("posY");
                    settlementRecord.Node.Attributes.Append(attribute2);
                }

                var attribute3 = settlementRecord.Node.Attributes["posY"];
                vec2 = settlementRecord.Position;
                var str2 = vec2.Y.ToString();
                attribute3.Value = str2;
                if (settlementRecord.HasGate)
                {
                    if (settlementRecord.Node.Attributes["gate_posX"] == null)
                    {
                        var attribute4 = settlementDocument.CreateAttribute("gate_posX");
                        settlementRecord.Node.Attributes.Append(attribute4);
                    }

                    var attribute5 = settlementRecord.Node.Attributes["gate_posX"];
                    vec2 = settlementRecord.GatePosition;
                    var str3 = vec2.X.ToString();
                    attribute5.Value = str3;
                    if (settlementRecord.Node.Attributes["gate_posY"] == null)
                    {
                        var attribute6 = settlementDocument.CreateAttribute("gate_posY");
                        settlementRecord.Node.Attributes.Append(attribute6);
                    }

                    var attribute7 = settlementRecord.Node.Attributes["gate_posY"];
                    vec2 = settlementRecord.GatePosition;
                    var str4 = vec2.Y.ToString();
                    attribute7.Value = str4;
                }
            }

            settlementDocument.Save(SettlementsXmlPath);
        }

        private void SaveSettlementDistanceCache()
        {
            BinaryWriter binaryWriter = null;
            try
            {
                var settlementRecordList = LoadSettlementData(LoadXmlFile(SettlementsXmlPath));
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Mountain),
                    false);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Lake), false);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Water), false);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.River), false);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Canyon), false);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.RuralArea),
                    false);
                binaryWriter = new BinaryWriter(File.Open(SettlementsDistanceCacheFilePath,
                    FileMode.Create));
                binaryWriter.Write(settlementRecordList.Count);
                for (var index1 = 0; index1 < settlementRecordList.Count; ++index1)
                {
                    binaryWriter.Write(settlementRecordList[index1].SettlementId);
                    var gatePosition1 = settlementRecordList[index1].GatePosition;
                    var record1 = new PathFaceRecord(-1, -1, -1);
                    Scene.GetNavMeshFaceIndex(ref record1, gatePosition1, false);
                    for (var index2 = index1 + 1; index2 < settlementRecordList.Count; ++index2)
                    {
                        binaryWriter.Write(settlementRecordList[index2].SettlementId);
                        var gatePosition2 = settlementRecordList[index2].GatePosition;
                        var record2 = new PathFaceRecord(-1, -1, -1);
                        Scene.GetNavMeshFaceIndex(ref record2, gatePosition2, false);
                        float distance;
                        Scene.GetPathDistanceBetweenAIFaces(record1.FaceIndex, record2.FaceIndex, gatePosition1,
                            gatePosition2, 0.1f, float.MaxValue, out distance);
                        binaryWriter.Write(distance);
                    }
                }
            }
            catch
            {
            }
            finally
            {
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Mountain), true);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Lake), true);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Water), true);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.River), true);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.Canyon), true);
                Scene.SetAbilityOfFacesWithId(MapScene.GetNavigationMeshIndexOfTerrainType(TerrainType.RuralArea),
                    true);
                binaryWriter?.Close();
            }
        }

        private XmlDocument LoadXmlFile(string path)
        {
            Debug.Print("opening " + path);
            var xmlDocument = new XmlDocument();
            var streamReader = new StreamReader(path);
            xmlDocument.LoadXml(streamReader.ReadToEnd());
            streamReader.Close();
            return xmlDocument;
        }

        protected override bool IsOnlyVisual()
        {
            return true;
        }

        private struct SettlementRecord
        {
            public readonly string SettlementName;
            public readonly string SettlementId;
            public readonly XmlNode Node;
            public readonly Vec2 Position;
            public readonly Vec2 GatePosition;
            public readonly bool HasGate;

            public SettlementRecord(
                string settlementName,
                string settlementId,
                Vec2 position,
                Vec2 gatePosition,
                XmlNode node,
                bool hasGate)
            {
                SettlementName = settlementName;
                SettlementId = settlementId;
                Position = position;
                GatePosition = gatePosition;
                Node = node;
                HasGate = hasGate;
            }
        }
    }
}