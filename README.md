<div id="top"></div>

<br />
<div align="center">
  <a href="https://gitlab.com/KingdomsOfArda/koa-source">
  </a>

<h2 align="center">Kingdoms of Arda Source</h2>
  <p align="center">
    A LOTR total-overhaul mod for Mount and Blade II Bannerlord
    <br />
    <a href="https://gitlab.com/KingdomsOfArda/koa-source">Getting Started (TODO)</a>
    ·
    <a href="https://gitlab.com/KingdomsOfArda/koa-source/issues">Report a Bug</a>
    ·
    <a href="https://gitlab.com/KingdomsOfArda/koa-source/issues">Request a Feature</a>
  </p>
</div>

<p>&nbsp;</p>

## Contributing


1. Create your branch (`git checkout -b feature/feature-name`). Note that your branch must be prefixed with either `feature/` or `bugfix/`.
2. Commit your Changes (`git commit -m 'Add a cool feature'`). Please keep commits atomic and name them concisely.
3. Push to the Branch (`git push origin feature/feature-name`)
4. Open a Pull Request
5. Address any provided feedback and await a Maintainer to approve your PR.

<p>&nbsp;</p>

<!-- LICENSE -->
## License

Distributed under the GNU AGPLv3 License. See the `LICENSE` file for more information.

<p>&nbsp;</p>

<!-- CONTACT -->
## Contact the Maintainers

Feel free to join the [KoA Discord](https://discord.gg/CzREb2eFvA) and send `@Jansen` or `@Huseyin` a message.

<p align="right">(<a href="#top">back to top</a>)</p>
