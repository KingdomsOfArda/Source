﻿using System.Collections.Generic;
using HarmonyLib;
using KoA.Campaign.MiddleEarthCampaign;
using KoA.Campaign.Models;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;
using TaleWorlds.Engine.GauntletUI;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade;

namespace KoA.Campaign
{
    public class SubModule : MBSubModuleBase
    {
        public static Harmony Patcher = new("KoA_Campaign");
        private bool _behaviorRemoved;

        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
        }

        protected override void OnSubModuleLoad()
        {
            Patcher.PatchAll();

            Module.CurrentModule.AddInitialStateOption(
                new InitialStateOption("CreateCampaign",
                    new TextObject("Begin a new journey"),
                    -100,
                    () => { MBGameManager.StartNewGame(new KoAGameManager()); },
                    () => (false, new TextObject("no reason (ツ)_/¯"))));

            UIResourceManager.UIResourceDepot.StartWatchingChangesInDepot();
        }

        protected override void OnApplicationTick(float dt)
        {
            //Enable gauntlet XML hot-reloading
            UIResourceManager.UIResourceDepot.CheckForChanges();

            // Can't use this on other MBSubModuleBase functions since the RegisterEvents function of behavior is called before.  
            if (_behaviorRemoved || TaleWorlds.CampaignSystem.Campaign.Current == null) return;
            TaleWorlds.CampaignSystem.Campaign.Current.CampaignBehaviorManager
                .RemoveBehavior<BackstoryCampaignBehavior>();
            TaleWorlds.CampaignSystem.Campaign.Current.CampaignBehaviorManager
                .RemoveBehavior<WorkshopsCampaignBehavior>();
            _behaviorRemoved = true;
        }

        protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
        {
            if (!(gameStarterObject is CampaignGameStarter)) return;

            _behaviorRemoved = false;
            var campaignGameStarter =
                (CampaignGameStarter)gameStarterObject;

            RemoveGameModels(campaignGameStarter);
        }

        private void RemoveGameModels(CampaignGameStarter campaignGameStarter)
        {
            var gamemodels = (List<GameModel>)campaignGameStarter.Models;
            gamemodels.RemoveAll(model => model.GetType() == typeof(DefaultBanditDensityModel));
            gamemodels.Add(new KoABanditDefaultDensityModel());

            gamemodels.RemoveAll(model => model.GetType() == typeof(DefaultClanTierModel));
            gamemodels.Add(new KoAClanTierModel());
        }
    }
}