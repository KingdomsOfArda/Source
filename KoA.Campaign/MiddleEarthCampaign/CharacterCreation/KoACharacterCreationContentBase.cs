﻿using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Actions;
using TaleWorlds.Core;
using TaleWorlds.ObjectSystem;

namespace KoA.Campaign.MiddleEarthCampaign.CharacterCreation
{
    public class KoACharacterCreationContentBase
    {
        private CultureObject _culture;

        public static KoACharacterCreationContentBase Instance =>
            !(GameStateManager.Current.ActiveState is KoACharacterCreationState activeState)
                ? null
                : activeState.CurrentCharacterCreationContent;

        public void Initialize()
        {
            SetMainHeroInitialStats();
        }

        public void ApplySkillAndAttributeEffects(
            List<SkillObject> skills,
            int focusToAdd,
            int skillLevelToAdd,
            CharacterAttribute attribute,
            int attributeLevelToAdd,
            List<TraitObject> traits = null,
            int traitLevelToAdd = 0,
            int renownToAdd = 0,
            int goldToAdd = 0,
            int unspentFocusPoints = 0,
            int unspentAttributePoints = 0)
        {
            foreach (var skill in skills)
            {
                Hero.MainHero.HeroDeveloper.AddFocus(skill, focusToAdd, false);
                if (Hero.MainHero.GetSkillValue(skill) == 1)
                    Hero.MainHero.HeroDeveloper.ChangeSkillLevel(skill, skillLevelToAdd - 1, false);
                else
                    Hero.MainHero.HeroDeveloper.ChangeSkillLevel(skill, skillLevelToAdd, false);
            }

            Hero.MainHero.HeroDeveloper.UnspentFocusPoints += unspentFocusPoints;
            Hero.MainHero.HeroDeveloper.UnspentAttributePoints += unspentAttributePoints;
            if (attribute != null)
                Hero.MainHero.HeroDeveloper.AddAttribute(attribute, attributeLevelToAdd, false);
            if (traits != null && traitLevelToAdd > 0 && traits.Count > 0)
                foreach (var trait in traits)
                    Hero.MainHero.SetTraitLevelInternal(trait, Hero.MainHero.GetTraitLevel(trait) + traitLevelToAdd);
            if (renownToAdd > 0)
                GainRenownAction.Apply(Hero.MainHero, renownToAdd, true);
            if (goldToAdd > 0)
                GiveGoldAction.ApplyBetweenCharacters(null, Hero.MainHero, goldToAdd, true);
            Hero.MainHero.HeroDeveloper.SetInitialLevel(1);
        }

        public void SetSelectedCulture(CultureObject culture)
        {
            _culture = culture;
        }

        public void ApplyCulture()
        {
            CharacterObject.PlayerCharacter.Culture = Instance._culture;
            Clan.PlayerClan.Culture = Instance._culture;
            Clan.PlayerClan.UpdateHomeSettlement(null);
            Hero.MainHero.BornSettlement = Clan.PlayerClan.HomeSettlement;
        }

        public IEnumerable<CultureObject> GetCultures()
        {
            foreach (var objectType in MBObjectManager.Instance.GetObjectTypeList<CultureObject>())
                if (objectType.IsMainCulture)
                    yield return objectType;
        }

        private void SetMainHeroInitialStats()
        {
            Hero.MainHero.HeroDeveloper.ClearHero();
            Hero.MainHero.HitPoints = 100;
            foreach (var skill in Skills.All)
                Hero.MainHero.HeroDeveloper.InitializeSkillXp(skill);
            foreach (var attrib in Attributes.All)
                Hero.MainHero.HeroDeveloper.AddAttribute(attrib, 2, false);
        }
    }
}