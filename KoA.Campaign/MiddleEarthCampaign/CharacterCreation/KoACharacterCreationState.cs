﻿using System.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.ObjectSystem;

namespace KoA.Campaign.MiddleEarthCampaign.CharacterCreation
{
    public class KoACharacterCreationState : PlayerGameState
    {
        public readonly KoACharacterCreationContentBase CurrentCharacterCreationContent;
        private bool _oldGameStateManagerDisabledStatus;

        public KoACharacterCreationState(KoACharacterCreationContentBase baseContent)
        {
            CurrentCharacterCreationContent = baseContent;
        }

        public KoACharacterCreationState()
        {
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            _oldGameStateManagerDisabledStatus = Game.Current.GameStateManager.ActiveStateDisabledByUser;
            Game.Current.GameStateManager.ActiveStateDisabledByUser = true;
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            Clan.PlayerClan.Renown = 0.0f;

            var str1 =
                "<BodyProperties version='4' age='25.84' weight='0.5000' build='0.5000'  key='000BAC088000100DB976648E6774B835537D86629511323BDCB177278A84F667017776140748B49500000000000000000000000000000000000000003EFC5002'/>";
            BodyProperties.FromString(str1, out var bodyProperties);
            CharacterObject.PlayerCharacter.UpdatePlayerCharacterBodyProperties(bodyProperties, false);

            var cultures = MBObjectManager.Instance.GetObjectTypeList<CultureObject>().Where(c => c.IsMainCulture);
            var defaultCulture = cultures.FirstOrDefault(c => c.StringId == "gondor");
            KoACharacterCreationContentBase.Instance.SetSelectedCulture(defaultCulture);
            KoACharacterCreationContentBase.Instance.ApplyCulture();
            TaleWorlds.CampaignSystem.Campaign.Current.PlayerTraitDeveloper.UpdateTraitXPAccordingToTraitLevels();
            FinalizeCharacterCreation();
        }

        public void FinalizeCharacterCreation()
        {
            Game.Current.GameStateManager.ActiveStateDisabledByUser = _oldGameStateManagerDisabledStatus;
            Game.Current.GameStateManager.CleanAndPushState(Game.Current.GameStateManager.CreateState<MapState>());
            PartyBase.MainParty.Visuals.SetMapIconAsDirty();
            CampaignEventDispatcher.Instance.OnCharacterCreationIsOver();
        }
    }
}