using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;

namespace KoA.Campaign.Models
{
    public class KoABanditDefaultDensityModel : DefaultBanditDensityModel
    {
        public override int NumberOfMaximumLooterParties => 10;
    }
}