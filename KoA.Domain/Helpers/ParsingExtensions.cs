﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using TaleWorlds.Library;

namespace KoA.Domain.Helpers
{
    public static class ParsingExtensions
    {
        public static int? AsNullableInt(this string value)
        {
            return !string.IsNullOrEmpty(value) ? int.Parse(value) : null;
        }

        public static float? AsNullableFloat(this string value)
        {
            return !string.IsNullOrEmpty(value) ? float.Parse(value) : null;
        }

        public static T AsNullableEnum<T>(this string value)
        {
            return !string.IsNullOrEmpty(value) ? (T)Enum.Parse(typeof(T), value) : default;
        }

        /// <summary>
        ///     Checks if the given string value can be cast into an enum value.
        /// </summary>
        /// <param name="value">The value that will be cast into an enum value</param>
        /// <param name="default">The value that will be used if the given string value is not an enum value.</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T AsNullableEnum<T>(this string value, T @default)
        {
            return !string.IsNullOrEmpty(value) ? (T)Enum.Parse(typeof(T), value) : @default;
        }

        [SuppressMessage("ReSharper", "PossibleInvalidOperationException")]
        public static Vec3? AsNullableVec3(this string value)
        {
            if (value == null) return null;

            var values = value.Split().Select(x => x.AsNullableFloat()).ToList();

            if (values.Any(x => x == null)) throw new ArgumentNullException(value, "Unable to parse Vec3 values");

            return new Vec3(values[0].Value, values[1].Value, values[2].Value, values[3].Value);
        }
    }
}