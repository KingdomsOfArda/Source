﻿using TaleWorlds.Core;
using TaleWorlds.Library;

namespace KoA.Domain.Helpers
{
    public static class TextHelper
    {
        public static void DisplayRedText(string message)
        {
            InformationManager.DisplayMessage(new InformationMessage(message, Color.ConvertStringToColor("#FF0000FF")));
        }
    }
}