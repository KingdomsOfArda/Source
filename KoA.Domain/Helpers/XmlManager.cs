using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using TaleWorlds.ObjectSystem;

namespace KoA.Core.Util
{
    public static class XmlManager
    {
        /// <summary>
        ///     Removes an XML file from a module at runtime. Use this as before a new Game is created.
        /// </summary>
        /// <param name="moduleId">The Id of the module in its SubModule.xml</param>
        /// <param name="xmlId">The Id attribute in the XMLName tag</param>
        /// <param name="xmlName">The name of the xml that you want to remove specifically within the XMLId</param>
        public static void RemoveXml(string moduleId, string xmlId, string xmlName = "", bool removeAllFound = true)
        {
            do
            {
                var index = XmlResource.XmlInformationList.FindIndex(x => x.ModuleName == moduleId &&
                                                                          x.Id == xmlId &&
                                                                          (xmlName == string.Empty ||
                                                                           x.Name == xmlName));
                if (index == -1)
                    return;
                XmlResource.XmlInformationList.RemoveAt(index);
            } while (removeAllFound);
        }

        public static void DeserializeDocument(XmlDocument doc)
        {
            MBObjectManager.Instance.LoadXml(doc, null);
        }

        public static XmlDocument LoadXmlDocument(string path)
        {
            var xmlReader = XmlReader.Create(path);
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlReader);
            return xmlDocument;
        }

        public static XmlDocument LoadXmlDocument(string xmlPath, string xsdPath)
        {
            var xmlSchemaSet = new XmlSchemaSet();
            xmlSchemaSet.Add("",
                new StringReader(xsdPath).ReadToEnd() ?? throw new ArgumentException("Given XSD Path is not valid"));

            var settings = new XmlReaderSettings();
            settings.Schemas.Add(xmlSchemaSet);
            settings.ValidationType = ValidationType.Schema;

            var xmlReader = XmlReader.Create(xmlPath, settings);
            var document = new XmlDocument();
            document.Load(xmlReader);


            return document;
        }

        public static XDocument LoadXDocument(string path)
        {
            var s = new StreamReader(path).ReadToEnd();
            return XDocument.Parse(s);
        }
    }
}